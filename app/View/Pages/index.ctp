<!-- ** Home Slider** -->
<div id="home-slider">
	<div class="slider-container">
    
    	<div class="slider-wrapper theme-default">    
            <div id="slider" class="nivoSlider">
                <img src="<?php echo $this->webroot; ?>asset/images/background1.jpg" alt="" title="#htmlcaption1" />
                <img src="<?php echo $this->webroot; ?>asset/images/background2.jpg" alt="" title="#htmlcaption2" />
                <img src="<?php echo $this->webroot; ?>asset/images/background3.jpg" alt="" title="#htmlcaption3" />
            </div>
            <div id="htmlcaption1" class="nivo-html-caption">
                 <h2> Get well with the natural healing force </h2>
                <p> Ayurveda triggers the natural healing force within each of us which is the greatest power that makes us healthy, wealthy and wise. </p>
                <!-- <a href="##" title=""> <span> Read More </span> </a> -->
            </div>
            <div id="htmlcaption2" class="nivo-html-caption">
                 <h2> Ayurveda- The most sacred science of love </h2>
                <p> Beneficial to the body, senses, mind and reincarnating soul. </p>
                <!-- <a href="##" title=""> <span> Read More </span> </a> -->
            </div>   
             <div id="htmlcaption3" class="nivo-html-caption">
                 <h2> follow our bliss and dive deeply into the mysteries of fragrance, color, and taste</h2>
                <p> lend with the magnificent diversity of mother nature; and follow the inner signs to become aware of who we really are </p>
                <<!-- a href="##" title=""> <span> Read More </span> </a> -->
            </div>    
                    
        </div>
        
    </div>
</div><!-- **Home Slider - End** -->

<!-- ** Main** -->
<div id="main">
	<!-- **Main Container** -->
	<div class="main-container">
    
    	<!-- **Content Full Width** -->
    	<div class="content content-full-width">
        
        	<!-- **Services** -->
        	<h1 class="title"> <span> Services </span> </h1>                     
            <div class="column one-fourth">
                <div class="content-center-aligned">   
                    <a href="##" title=""> <span class="arctext" data-radius="240">Rejuvenation </span> <span class="rounded-img border"> <img src="<?php echo $this->webroot; ?>asset/images/Rejuvenation.jpg" alt="" title="" /> </span> </a>
                    <p> Rejuvenation Therapy (Rasayana Chikitsa) in Ayurveda aids in toning up the skin and strengthening body tissues so as to facilitate longevity.  </p>
                </div>
            </div>
            <div class="column one-fourth">
                <div class="content-center-aligned">   
                    <a href="##" title=""> <span class="arctext" data-radius="240">Weight Manage </span> <span class="rounded-img border"> <img src="<?php echo $this->webroot; ?>asset/images/weightloss.jpg" alt="" title="" /> </span> </a>
                    <p> Ayurveda treatments are natural and healthier than weight loss pills or crash diets.   </p>
                </div>
            </div>
            <div class="column one-fourth">
                <div class="content-center-aligned">   
                    <a href="##" title=""> <span class="arctext" data-radius="240">Bridal Makeover </span> <span class="rounded-img border"> <img src="<?php echo $this->webroot; ?>asset/images/bridal.jpg" alt="" title="" /> </span> </a>
                    <p> A comprehensive set of head to toe treatments that is ideal to use especially three months before the wedding.   </p>
                </div>
            </div>
            <div class="column one-fourth last">
                <div class="content-center-aligned">   
                    <a href="##" title=""> <span class="arctext" data-radius="240">NRI Package </span> <span class="rounded-img border"> <img src="<?php echo $this->webroot; ?>asset/images/nri.png" alt="" title="" /> </span> </a>
                    <p> Will Make You Mentally and Physically Stress Free to Create Healthy Body Which create The Healthy Mind and make You Energetic.   </p>
                </div>
            </div>
            <!-- **Services - End** -->
            
            <div class="hr_invisible"> </div>
            
            <!-- **Popular Procedures** -->
            <h1 class="title"> <span> Popular Procedures </span> </h1>
            
            <div class="column one-half">
                <div class="box-content">
                    <a href="##" title=""> <img src="<?php echo $this->webroot; ?>asset/images/wellness.jpg" alt="" title="" class="alignleft" /> </a>
                    <h2>WELLNESS TREATMENTS: </h2>
                    <p> Wellness is a state of being healthy where the body, mind and soul are in a perfect state of equilibrium, making one feel healthy and wholesome. </p>
                    <a href="##" title="Read More" class="tooltip-top readmore"> </a> 
                </div>
            </div>
            
            <div class="column one-half last">
                <div class="box-content">
                    <a href="##" title=""> <img src="<?php echo $this->webroot; ?>asset/images/fourhandtherappy.jpg" alt="" title="" class="alignleft" /> </a>
                    <h2>Four Hand Therapy</h2>
                    <p> During a regular massage the mind may try to follow the movements of the hands of the therapist.When 4 hands are used this becomes impossible which causes the mind to give up control, allowing a deeper relaxation.   </p>
                    <a href="##" title="Read More" class="tooltip-top readmore"> </a> 
                </div>
            </div>
            <!-- **Popular Procedures - End** -->
            
             <div class="hr_invisible"> </div>
            

             <!--**Three Column** -->
            <div class="column one-third">
                <h4 class="title"> <span> Other Services </span> </h4>
                <ul class="flower-bullet green">
                    <li> <a href="" title=""> NAVARAKIZHI </a> </li>
                    <li> <a href="" title=""> UDWARTHANAM </a> </li>
                    <li> <a href="" title=""> KATI VASTI </a> </li>
                    <li> <a href="" title=""> SHIRA MARDANA </a> </li>
                    <li> <a href="" title=""> ABHYANAGAM </a> </li>
                    <li> <a href="" title=""> SHIRODHARA </a> </li>
                    <li> <a href="" title=""> TAKRA DHARA </a> </li>
                    <li> <a href="" title=""> KIZHI </a> </li>
                </ul>
            </div>
            <div class="column one-third">
            	<h4 class="title"> <span> Gift a Friend </span> </h4>
                <a href="" title=""> <img src="<?php echo $this->webroot; ?>asset/images/gift-card.png" alt="" title="" /> </a>
            </div>
            <div class="column one-third last">
                <h4 class="title"> <span> Working Hours </span> </h4>
                <div class="notice"> <span class="left"> Mon - Fri : </span> <span class="right"> 8am - 6pm </span> </div>
                <div class="notice"> <span class="left"> Sat &amp; Sunday : </span> <span class="right"> 8am - 10pm </span> </div>
                <p> <strong> Note: </strong> Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In in massa urna, vitae vestibulum orci.  </p>
            </div> 
            <!-- **Three Column - End** -->
            
        </div> <!-- **Content Full Width - End** -->   	