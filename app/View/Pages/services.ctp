    
    <!-- **Breadcrumb** -->
    <div class="breadcrumb">
        <div class="container">
            <a href="<?php echo $this->webroot; ?>" title=""> Home </a>
            <span class="arrow"> </span>
            <span class="current-crumb"> Services </span>
        </div>  <!-- **Breadcrumb - End** -->           
    </div>
    <!-- **Main Container** -->
    <div class="main-container">
    
        <!-- **Content Full Width** -->
        <div class="content content-full-width">  
            <!-- <p>  Our services as follows. </p>  -->
            
            <div class="hr_invisible"> </div>
            
            <div class="column one-fourth">            
                <a href="" title="" class="rounded-img border alignleft"> <img src="<?php echo $this->webroot; ?>asset/images/services/img1.jpg" alt="" title="" /> </a>
            </div>
            
            <div class="column three-fourth last">
                <h1> NAVARAKIZHI </h1>
                <h3>Bundle nourishing therapy</h3>
                <p> Milk processed with herbs combined with special rice known as Navara used as an effective therapy. Cleaned navara rice is boiled in a mixture of milk then tied into a cloth bundle and massaged all over the body. This treatment helps in strengthening the nerves and relieving arthritis.
                24 hour advanced reservation required for preparation

                </p>
            </div>
            
            <div class="hr"> </div>            
            
            
            <div class="column three-fourth">
                <div class="content-right-aligned">
                    <h1> UDWARTHANAM </h1>
                    <h3 style="text-align: right;">Herbal powder massage</h3>
                    <p> Astimulating massage that uses mildly heated dry herbal powder that exfoliates your body and helps break down fat deposits. This unique therapy improves circulation, reduces cellulite accumulation and enhances skin texture and appearance. It is also helpful in eliminating body odour.</p>
                </div>
            </div>
            
            <div class="column one-fourth last">
                <a href="" title="" class="rounded-img border alignright"> <img src="<?php echo $this->webroot; ?>asset/images/services/img2.jpg" alt="" title="" /> </a>
            </div>
            
            <div class="hr"> </div>
            
            <div class="column one-fourth">
                <a href="" title="" class="rounded-img border alignleft"> <img src="<?php echo $this->webroot; ?>asset/images/services/img3.jpg" alt="" title="" /> </a>
            </div>
            <div class="column three-fourth last">
                <h1> KATI VASTI </h1>
                <h3>Back and joint pain relief</h3>
                <p> Kati vasti is a highly effective treatment for back pain and spinal disorders which utilise the retention of warm thick medicated oil and massaged over the lower back or other parts of the spine for a certain period. This treatment helps relieve pain and numbness. It also helps increases blood circulation, strengthens muscles and connecting tissues.</p>
            </div>
            
            <div class="hr"> </div>
            
                       
            <div class="column three-fourth">    
                <div class="content-right-aligned">
                    <h1> SHIRA MARDANA </h1>
                    <h3 style="text-align: right;">Ayurvedic head massage</h3>
                    <p> Based on an ancient healing system, shira mardana is an oil head message that use specific pressure point techniques to relax and revitalise the mind and entice the senses.</p>
                </div>
            </div>  
            
            <div class="column one-fourth last">            
                <a href="" title="" class="rounded-img border alignright"> <img src="<?php echo $this->webroot; ?>asset/images/services/img4.jpg" alt="" title="" /> </a>
            </div>     


           


            <div class="hr"> </div>  

             <div class="column one-fourth">            
                <a href="" title="" class="rounded-img border alignleft"> <img src="<?php echo $this->webroot; ?>asset/images/services/img5.jpg" alt="" title="" /> </a>
            </div>
            <div class="column three-fourth last">
                <h1> ABHYANAGAM </h1>
                <h3>The synchronised massage</h3>
                <p> Abhyanagam in Sanskrit means oil application. This relaxing and refreshing full body massage is a masterpiece of KeralaAyurveda. It is a friction based massage technique performed by two therapists in a synchronized manner using herb influenced therapeutic oil. It imparts softness of the skin, nourishes the body, improves vision and decreases the effects of aging</p>
            </div>
            
            <div class="hr"> </div>            
            
            
            <div class="column three-fourth">
                <div class="content-right-aligned">
                    <h1> SHIRODHARA </h1>
                    <h3 style="text-align: right;">The stream of oil</h3>
                    <p> Shirodhara is a union of two words- SHIRO (HEAD) AND Dhara (stream).Thus, shirodhara means pouring of therapeutic lukewarm oil on the forehead from a specific height and for a specific period continuously and uniformly in arhythm allowing the oil to run through the scalp and into the hair. Shirodhara works on the cerebral system, it improves the function of the five senses and helps reduce stress, anxiety, hair loss and fatigue</p>
                </div>
            </div>
            
            <div class="column one-fourth last">
                <a href="" title="" class="rounded-img border alignright"> <img src="<?php echo $this->webroot; ?>asset/images/services/img6.jpg" alt="" title="" /> </a>
            </div>


             <div class="hr"> </div>  
             <div class="column one-fourth">            
                <a href="" title="" class="rounded-img border alignleft"> <img src="<?php echo $this->webroot; ?>asset/images/services/img7.jpg" alt="" title="" /> </a>
            </div>
            <div class="column three-fourth last">
                <h1> TAKRA DHARA </h1>
                <h3>The buttermilk therapy</h3>
                <p>Takra(medicated buttermilk)Dhara (stream) is a combination of herbs infused buttermilk which produces a cooling effect on the brain and the nervous system. This ancient Ayurvedic therapy also helps in releasing stress.
                24 hour advanced reservation required for preparation.
                </p>
            </div>
            
            <div class="hr"> </div>            
            
            
            <div class="column three-fourth">
                <div class="content-right-aligned">
                    <h1> KIZHI </h1>
                    <h3 style="text-align: right;">Herbal bundle massage</h3>
                    <p> Available in options of powder  or leaf bundles,kizhi is one of the most common Ayurvedic pain relief techniques.This therapy is beneficial in reducing inflammation of spinal muscles,lower backaches,cervicial spondylitis,frozen shoulders as well as eliminating toxins.
                    24 hour advanced reservation required for preparation.</p>
                </div>
            </div>
            
            <div class="column one-fourth last">
                <a href="" title="" class="rounded-img border alignright"> <img src="<?php echo $this->webroot; ?>asset/images/services/img8.jpg" alt="" title="" /> </a>
            </div>

            <div class="hr"> </div>  
        </div> <!-- **Content Full Width - End** -->    
        
        
    </div><!-- **Main Container - End** -->