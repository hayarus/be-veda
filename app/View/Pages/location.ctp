    
    <!-- **Breadcrumb** -->
    <div class="breadcrumb">
        <div class="container">
            <a href="<?php echo $this->webroot; ?>" title=""> Home </a>
            <span class="arrow"> </span>
            <span class="current-crumb"> Location </span>
        </div>  <!-- **Breadcrumb - End** -->           
    </div>

    <!-- **Main Container** -->
    <div class="main-container">
    
        <!-- **Content Full Width** -->
        <div class="content content-full-width"> 
        
            <div class="green-border">

             <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15717.553931644794!2d76.29937081383974!3d9.984733185784847!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b080d2678058027%3A0xb3fa9bafefeb4c44!2sThammanam%2C+Ernakulam%2C+Kerala%2C+India!5e0!3m2!1sen!2smy!4v1534416048488" width="930" height="340" frameborder="0" style="border:0" allowfullscreen></iframe> </div>
            
            <div class="hr_invisible"> </div>
            
            <div class="column one-third">              
                <h1> Contact Details </h1>  
               <ul class="contact-details">   
                        <li> <span class="address"> </span> <p> Thammanam P.O <br />  cochin 32 <br />  </p> </li>             
                        <li> <span class="mail"> </span> <p> <a href="" title="">info@bevedaindia.com</a> </p> </li>
                        <li> <span class="phone"> </span> <p> +91 8089 043  433</p> </li>                        
                    </ul>
            </div>
            
            <div class="column one-third">
                <h1> Working Hours </h1>
                <div class="notice"> <span class="left"> Mon - Fri : </span> <span class="right"> 8am - 6pm </span> </div>
                <div class="notice"> <span class="left"> Sat &amp; Sunday : </span> <span class="right"> 8am - 10pm </span> </div>
                <a href="<?php echo $this->webroot; ?>pages/book_now" class="big-ico-button red book"> <span> Book an Appointment </span> </a> 
            </div>
            
            <div class="column one-third last">
                <div class="enquiry-form">
                    <h1> Enquiry </h1>
                    <div id="ajax_message"></div>
                     <form name="frmcontact" id="frmcontact" action="php/contact.php" method="post">
                        <input id="name" name="name" type="text" value="Name"
                            onblur="this.value=(this.value=='') ? 'Name' : this.value;" onfocus="this.value=(this.value=='Name') ? '' : this.value;" />
                        <input id="email" name="email" type="text" value="Email" 
                            onblur="this.value=(this.value=='') ? 'Email' : this.value;" onfocus="this.value=(this.value=='Email') ? '' : this.value;" />
                        <textarea id="message" name="message"
                            onblur="this.value=(this.value=='') ? 'Message' : this.value;" onfocus="this.value=(this.value=='Message') ? '' : this.value;" cols="" rows="">Message</textarea>
                        <input name="submit" id="send" type="submit" value="Message" />
                    </form>
                </div>
            </div>
            
            
        </div> <!-- **Content Full Width - End** -->    
        
       
        
    </div><!-- **Main Container - End** -->