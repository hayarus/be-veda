<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title> Be Veda </title>

<!-- **Favicon** -->
<link href="<?php echo $this->webroot; ?>asset/favicon.ico" rel="shortcut icon" type="image/x-icon" />

<!-- **CSS - stylesheets** -->
<link href="<?php echo $this->webroot; ?>asset/style.css" rel="stylesheet" type="text/css" media="all" />

<!-- **Additional - stylesheets** -->
<link href="<?php echo $this->webroot; ?>asset/responsive.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo $this->webroot; ?>asset/css/nivo-slider.css" rel="stylesheet"  type="text/css" media="screen"/>

</head>

<body class="home">

<!-- **Header** -->
<div id="header">
	<div class="container">   	

<!--**top menu**-->

        <?php echo $this->element('menubar'); ?> 

<!--**top menu end**-->


        <!-- **Logo** -->
        <div id="logo">
        	<a href="<?php echo $this->webroot; ?>" title=""> <img src="<?php echo $this->webroot; ?>asset/images/logo.png" alt="" title="" /> </a>
        </div><!-- **Logo - End** -->
        
        <!-- **Searchform** -->
        <form action="#" id="searchform" method="get">
        	<fieldset>
                <input name="Search" type="text" onblur="this.value=(this.value=='') ? 'Enter Keyword' : this.value;" onfocus="this.value=(this.value=='Enter Keyword') ? '' : this.value;" value="Enter Keyword" alt="Search our site" class="text_input" />
                <input name="submit" type="submit" class="button" value="" />
            </fieldset>
        </form><!-- **Searchform - End** -->
    	
    </div>
</div><!-- **Header - End** -->


<!--**content**-->

<?php echo $this->fetch('content'); ?>

<!--**content end**-->
        
        <!-- **Newsletter** -->
        <!-- <div id="newsletter">
        	<h2> Subscribe to Newsletter </h2>
            <form action="#" method="get">
            	<input name="name" type="text" onblur="this.value=(this.value=='') ? 'Enter Name' : this.value;" onfocus="this.value=(this.value=='Enter Name') ? '' : this.value;" value="Enter Name" />
                <input name="name" type="text" onblur="this.value=(this.value=='') ? 'Enter Email Address' : this.value;" onfocus="this.value=(this.value=='Enter Email Address') ? '' : this.value;" value="Enter Email Address" />
                <input name="submit" type="submit" value="Subscribe" />
            </form>
        </div> --><!-- **Newsletter - End** -->
        
    </div><!-- **Main Container - End** -->
    
    <!-- ** Footer** -->
    <div class="hr_invisible"> </div>  
    <div id="footer">
    	<div class="main-container">
        
        	<!-- **Recent Entries** -->
        	<div class="column one-fourth">
            	<div class="widget widget_recent_entries">
                    <h2 class="widgettitle"> <span> Latest Posts </span> </h2>
                    <ul>
                    	<li> 
                        	<a href="##" title=""> <img src="http://placehold.it/54x54" alt="" title="" /> </a>
                            <h6> <a href="##" title=""> Living In Style </a> </h6>
                            <p> Nulla ullamcorper faucibus tellus sed malesuada. </p>
                        </li>
                        <li> 
                        	<a href="##" title=""> <img src="http://placehold.it/54x54" alt="" title="" /> </a>
                            <h6> <a href="##" title=""> Healthy Mind and ... </a> </h6>
                            <p> In convallis hendrerit velit id vulputat. </p>
                        </li>
                        <li> 
                        	<a href="##" title=""> <img src="http://placehold.it/54x54" alt="" title="" /> </a>
                            <h6> <a href="##" title=""> Living In Style </a> </h6>
                            <p> Duis scelerisque, metus ac adipiscing nvallis. </p>
                        </li>
                    </ul>
                </div>
            </div><!-- **Recent Entries - End** -->
            
            <!-- **Resources** -->
            <div class="column one-fourth">
            	<div class="widget">
                    <h2 class="widgettitle"> <span> Resources </span> </h2>
                    <ul>
                        <li> <a href="" title=""> Spa Guide - The Complete </a> </li>
                        <li> <a href="" title=""> Salon Guide </a> </li>
                        <li> <a href="" title=""> Yoga Guide </a> </li>
                        <li> <a href="" title=""> Pilates Guide </a> </li>
                        <li> <a href="" title=""> Wellness Guide </a> </li>
                        <li> <a href="" title=""> Fitness Guide </a> </li>
                        <li> <a href="" title=""> Readers' Choice Awards </a> </li>
                        <li> <a href="" title=""> Club Spa Blog </a> </li>
                        <li> <a href="" title=""> SpaEvidence </a> </li>
                    </ul>
                </div>
            </div><!-- **Resources - End** -->
            
            <!-- **Testimonials** -->
            <div class="column one-fourth">
            	<div class="widget">
                    <h2 class="widgettitle"> <span> Testimonials </span> </h2>
                    
                    <div class="testimonial-skin-carousel">
                        <ul class="testimonial-carousel">
                            <li>
                                <blockquote>                                
                                    Nulla ullamcorper faucibus tellus sed malesuada. In convallis hendrerit velit id vulputate. In hac habitasse platea dictumst. Donec eget consequat urna. Pellentesque ac 
                                    nibh risus. Duis scelerisque, metus ac adipiscing convallis, nunc lectus commodo leo, a vulputate nisi elit in nisl. 
                                    <span> - Emmy Rossum </span>
                                </blockquote>
                            </li>
                            <li>
                                <blockquote>
                                	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed sapien ac risus pulvinar consequat. Nulla a urna a nibh consectetur semper quis hendrerit felis. 
                                    Aliquam sodales, elit sit amet pellentesque dapibus. 
                                    <span> - Aliquam erat </span>
                                </blockquote>
                            </li>
                            <li>
                                <blockquote>
                                    Proin magna diam, mollis vel pretium et, mattis ac erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. 
                                    Quisque in eros at nisi malesuada porttitor. Aenean blandit ornare urna at hendrerit. 
                                    <span> - Quisque vehicula </span>
                                </blockquote>
                            </li>
                            <li>
                                <blockquote>
                                    Nunc arcu magna, egestas non gravida vel, condimentum eu elit. Morbi egestas erat a urna mollis malesuada. Aliquam quis orci sem, ut facilisis massa. Suspendisse varius nisi vitae quam porta convallis. Aliquam ac quam neque, quis feugiat est. Praesent odio diam.
                                    <span> - Aenean malesuada </span>
                                </blockquote>
                            </li>
                        </ul> 
                    </div>
                    
                </div>
            </div><!-- **Testimonials - End** -->
            

            <!-- **Contact Us** -->
            <div class="column one-fourth last">
            	<div class="widget">
                    <h2 class="widgettitle"> <span> Contact Us </span> </h2>
                    <ul class="contact-details">   
                        <li> <span class="address"> </span> <p> Thammanam P.O <br />  cochin 32 <br />  </p> </li>             
                        <li> <span class="mail"> </span> <p> <a href="" title="">info@bevedaindia.com</a> </p> </li>
                        <li> <span class="phone"> </span> <p> +91 8089 043  433</p> </li>                        
                    </ul>                                        
                </div>
                <div class="widget social-widget">
                	<h2> <span> We're Social </span> </h2>
                    <ul>
                    	<li> 
                        	<a href="" title=""> 
                            	<img src="<?php echo $this->webroot; ?>asset/images/facebook-hover.png" alt="image" title="" />
                            	<img src="<?php echo $this->webroot; ?>asset/images/facebook.png" alt="image" title="" />                                
                            </a> 
                        </li>
                    	<li> 
                        	<a href="" title=""> 
                            	<img src="<?php echo $this->webroot; ?>asset/images/twitter-hover.png" alt="image" title="" />
                            	<img src="<?php echo $this->webroot; ?>asset/images/twitter.png" alt="image" title="" />                                
                            </a> 
                        </li>
                    	<li> 
                        	<a href="" title=""> 
                            	<img src="<?php echo $this->webroot; ?>asset/images/flickr-hover.png" alt="" title="" />
                            	<img src="<?php echo $this->webroot; ?>asset/images/flickr.png" alt="image" title="" />                                
                            </a> 
                        </li>
                    	<li> 
                        	<a href="" title=""> 
                            	<img src="<?php echo $this->webroot; ?>asset/images/youtube-hover.png" alt="" title="" />
                            	<img src="<?php echo $this->webroot; ?>asset/images/youtube.png" alt="image" title="" />                                
                            </a> 
                        </li>
                    </ul>  
                </div>
            </div><!-- **Contact Us - End** -->
        
        
        </div>
    </div><!-- **Footer - End** -->
    
    <!-- **Footer Bottom** -->
    <div class="footer-bottom"> 
    	<div class="main-container">        
        	<p> &copy; Be Veda <?php echo date('Y'); ?> </p>        
        </div>
    </div><!-- **Footer Bottom - End** -->

</div><!-- **Main - End**-->


<!-- **jQuery** -->
<script type="text/javascript" src="<?php echo $this->webroot; ?>asset/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>asset/js/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>asset/js/jquery.arctext.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>asset/js/jquery.jcarousel.min.js"></script>
<script src="<?php echo $this->webroot; ?>asset/js/responsive-nav.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>asset/js/jquery.meanmenu.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>asset/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>asset/js/spa.booknow.js"></script>
<script src="<?php echo $this->webroot; ?>asset/js/contact.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$("span.arctext").each(function(){
			$(this).arctext({radius: $(this).attr('data-radius')});
		});
	});
</script>

<script type="text/javascript" src="<?php echo $this->webroot; ?>asset/js/jquery.nivo.slider.js"></script>
<script src="<?php echo $this->webroot; ?>asset/js/jquery.tipTip.minified.js" type="text/javascript"></script>
<script type='text/javascript' src="<?php echo $this->webroot; ?>asset/js/spa.custom.js"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b75466bf31d0f771d83d8e3/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
