<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	//public $uses = array();

	public function beforeFilter() {
		
		parent::beforeFilter();
		$this->layout='frontend';
		
	}

	public function index() {

		
	}

	public function services() {

		
	}

	public function book_now() {

		$to ='akshaya@hayarus.com';
		$message ='<html><body>';
		$message .= '<p>First Name: '. $this->request->data('fname') .'<p></br> <p>Last Name: '. $this->request->data('lname') .'<p></br> <p>Gender: '. $this->request->data('Gender') .'<p></br> <p>Telephone: '. $this->request->data('phone') .'<p></br> <p>Email: '. $this->request->data('email').'<p></br> <p>Address: '. $this->request->data('address') .'<p></br> <p>Date of Treatment: '. $this->request->data('treatment_day').'/'. $this->request->data('treatment_month').'/'. $this->request->data('treatment_year') .'<p></br> <p>Preferred Time: '. $this->request->data('PreferredTime') .'<p></br> <p>Type of Treatment: '. $this->request->data('treatment') .'<p></br> <p>No. of Persons: '. $this->request->data('persons') .'<p></br> <p>Special Requests: '. $this->request->data('requests') .'<p></br>';

		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type:text/html;charset=UTF-8' . "\r\n";
		$headers .= '<p>From: '. $this->request->data('email').'<p></br>';
		$message .= '</body></html>';

		mail($to,$message,$headers);

		echo $message;exit;	
		
	}

	public function location() {

		$to ='akshaya@hayarus.com';
		$message ='<html><body>';
		$message .= '<p>Name: '. $this->request->data('name') .'<p></br> <p>Email: '. $this->request->data('email') .'<p></br> <p>Message: '. $this->request->data('message') .'<p></br>';

		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type:text/html;charset=UTF-8' . "\r\n";
		$headers .= '<p>From: '. $this->request->data('email').'<p></br>';
		$message .= '</body></html>';

		mail($to,$message,$headers);

		echo $message;exit;	

		
	}


}
